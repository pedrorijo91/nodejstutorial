var fs = require("fs")
var path = require("path")

module.exports = function(dir, ext, callback) {
	var array = [];

	fs.readdir(dir, function(err, files) {
			if(err) {
				return callback(err)
			}
			else {
				for(var i = 0; i < files.length; i++) {
					if(path.extname(files[i]) == ("." + ext)) {
						array.push(files[i])	
					}
				}
				callback(null, array)
			}
	})
}
