var net = require('net')

var server = net.createServer(function callback (socket) { 
				var date = new Date()

    				var year = date.getFullYear().toString()
       				var month = (date.getMonth() + 1).toString()     // starts at 0
	    			var day = date.getDate().toString()      // returns the day of month
	     			var hour = date.getHours().toString()
		    		var minute = date.getMinutes().toString()

				var formatedDate = year + "-" + month + "-" + day + " " + hour + ":" + minute
				socket.end(formatedDate)	
				})
server.listen(process.argv[2])

